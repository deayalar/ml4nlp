""" Topic Classification -- SVM tutorial

Usage:
  classification.py --C=<n> --kernel=[linear|poly|rbf] [--degree=<n>]
  classification.py (-h | --help)
  classification.py --version

Options:
  -h --help                             Show this screen.
  --version                             Show version.
  --C=<n>                               Value of C parameter.
  --kernel=[linear|poly|rbf]            Type of kernel.
  --degree=<n>                          Degree of kernel.

"""

import os
from sys import argv
from docopt import docopt
from numpy import concatenate
from sklearn.svm import SVC
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from utils import *

def train_svm(C, kernel, degree, key_to_index, train=False):
  #args = docopt(__doc__, version='Topic Classification 1.0')
  #print(args)
  #C       = int(args["--C"])
  #kernel  = args["--kernel"]
  #degree  = int(args["--degree"]) if args["--degree"] else 3

  #csv sources
  files = ["./data/class1/vecs.csv","./data/class2/vecs.csv"]

  #fetch data in csv and structure in a class:vector dict
  cl_dict = get_data(files)
  #print(cl_dict)
  t1 = list(cl_dict.keys())[0]
  t2 = list(cl_dict.keys())[1]

  #get user-selected size of training sets
  train1_size = get_train_size(t1, cl_dict[t1])
  train2_size = get_train_size(t2, cl_dict[t2])
  if not train:
    return None, None, train1_size, train2_size

  #build numpy arrays and lists of docs
  t1_train, t1_test, t1_train_docs, t1_test_docs, t1_train_indexes, t1_test_indexes = \
    make_arrays(cl_dict[t1], train1_size, key_to_index['./data/class1'])
  t2_train, t2_test, t2_train_docs, t2_test_docs, t2_train_indexes, t2_test_indexes = \
    make_arrays(cl_dict[t2], train2_size, key_to_index['./data/class2'])
  
  with open('train1', 'w') as f:
    for item in t1_train_docs:
        f.write("%s\n" % item)

  with open('train2', 'w') as f:
    for item in t2_train_docs:
        f.write("%s\n" % item)
  
  with open('test1', 'w') as f:
    for item in t1_test_docs:
        f.write("%s\n" % item)

  with open('test2', 'w') as f:
    for item in t2_test_docs:
        f.write("%s\n" % item)
        
  train_docs = t1_train_docs + t2_train_docs

  test1_size = len(t1_test)
  test2_size = len(t2_test)

  print('Topic 1: Train size: {} | Test size: {}\n' \
    .format(train1_size, test1_size) + \
    'Topic 2: Train size: {} | Test size: {}\n' \
    .format(train2_size, test2_size))
    
  #prepare train/test sets
  x_train = concatenate([t1_train, t2_train])
  x_test  = concatenate([t1_test,  t2_test])
  y_train = make_labels(train1_size, train2_size)
  y_test  = make_labels(test1_size,  test2_size)


  #scores = ['accuracy', 'precision_macro', 'recall_macro', 'f1_macro']
  scores = []
  # Set the parameters by cross-validation
  tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                      'C': [1, 10, 100, 1000]},
                      {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
                      {'kernel': ['poly'], 'degree':[2, 3], 'C': [1, 10, 100, 1000]}]

  for score in scores:
      print("# Tuning hyper-parameters for %s" % score)
      clf = GridSearchCV(
          SVC(verbose = False), tuned_parameters, scoring=score
      )
      clf.fit(x_train, y_train)

      print("Best parameters set found on development set:")
      print(clf.best_params_)
      print("Grid scores on development set:")
      means = clf.cv_results_['mean_test_score']
      stds = clf.cv_results_['std_test_score']
      for mean, std, params in zip(means, stds, clf.cv_results_['params']):
          print("%0.3f (+/-%0.03f) for %r"
                % (mean, std * 2, params))
      print()

      print("Detailed classification report:")
      print()
      print("The model is trained on the full development set.")
      print("The scores are computed on the full evaluation set.")
      print()
      y_true, y_pred = y_test, clf.predict(x_test)
      print(classification_report(y_true, y_pred))
      print()

  #setup SVM setup and print output
  print('SVC output:')
  clf    = SVC(C=C, gamma=0.001, verbose = True, kernel = kernel, degree = degree) #prints data
  model  = clf.fit(x_train, y_train)
  score  = clf.score(x_test, y_test)
  y_pred = clf.predict(x_test)

  print('\n') #needed because SVC prints output in a weird way
  print('SVC Model:')
  print(model)

  print('Score: {}\n'.format(score))

  #print('Training docs:')
  #print('\n'.join([train_docs[s] for s in clf.support_]))
  #print()

  #make confusion matrix
  make_confmat(y_pred, y_test, t1, t2) #prints some data

  precision = precision_score(y_test, y_pred, average='macro', zero_division=1)
  recall = recall_score(y_test, y_pred, average='macro', zero_division=1)
  f1 = f1_score(y_test, y_pred, average='macro', zero_division=1)

  print('Precision score: {0:0.2f}'.format(precision))
  print('Recall score: {0:0.2f}'.format(recall))
  print('F1 score: {0:0.2f}'.format(f1))
  return model, score, train1_size, train2_size

