import re
import json
import numpy as np
from numpy import save
from numpy import concatenate
import tensorflow as tf #tf v2.3.0 in eager mode
import tensorflow_hub as hub
import pandas as pd
import xml.etree.ElementTree as ET

from utils import *
from os import listdir
from scipy import stats
from os.path import isfile, isdir, join
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split, GridSearchCV

def get_sentences(xml_file):
  tree = ET.parse(xml_file)
  root = tree.getroot()
  sentences = []
  for child in root:
    text = child.text
    text = text.strip()
    text = text.replace("\n", " ")
    text = text.replace("  ", " ")
    text = text.replace("   ", " ")
    sentences.append(text)
  return sentences

USE_MODEL = "https://tfhub.dev/google/universal-sentence-encoder-large/5"

class1 = "data/class1/linear.txt"
class2 = "data/class2/linear.txt"

def get_embeddings(class_name, encoder, size=10):
  sentences_class = get_sentences(join("data", class_name, "linear.txt"))
  print(len(sentences_class))

  all_embeddings = np.empty([0, 512])
  start = 0

  while start < (len(sentences_class) - size):
      stop = start + size
      print(str(start) + "," + str(stop))
      embeddings = embed(sentences_class[start:stop]).numpy()
      all_embeddings = np.concatenate((all_embeddings, embeddings), axis=0)
      start = stop

  print(all_embeddings.shape)
  save(class_name, all_embeddings)

def extract_embeddings1(filename, key_to_index, class_name, embeddings):
  filename = open(filename, 'r')
  keys = filename.readlines()
  indexes = [key_to_index[class_name][i.strip("\n")] for i in keys if key_to_index[class_name][i.strip("\n")] < len(embeddings)]
  return embeddings[indexes]

if not isfile("class1.npy") and not isfile("class2.npy"):
  embed = hub.load(USE_MODEL)
  get_embeddings("class1", embed)
  get_embeddings("class2", embed)

key_to_index = json.load(open("key_to_index.json"))

embeddings1 = np.load('class1.npy')
train1_embeddings = extract_embeddings1("train1", key_to_index, "./data/class1", embeddings1)
test1_embeddings = extract_embeddings1("test1", key_to_index, "./data/class1", embeddings1)

embeddings2 = np.load('class2.npy')
train2_embeddings = extract_embeddings1("train2", key_to_index, "./data/class2", embeddings2)
test2_embeddings = extract_embeddings1("test2", key_to_index, "./data/class2", embeddings2)

X_train = concatenate([train1_embeddings, train2_embeddings])

y_train = []
for i in range(0, len(train1_embeddings)):
    y_train.append("1") #Positive examples in class1
for i in range(0, len(train2_embeddings)):
    y_train.append("0") #Negative examples in class2

X_test = concatenate([test1_embeddings, test2_embeddings])
y_test = []
for i in range(0, len(test1_embeddings)):
    y_test.append("1") #Positive examples in class1
for i in range(0, len(test2_embeddings)):
    y_test.append("0") #Negative examples in class2

print(X_train.shape)
print(X_test.shape)
print(len(y_train)) 
print(len(y_test))

# Set the parameters by cross-validation
tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
                    {'kernel': ['poly'], 'degree':[2, 3], 'C': [1, 10, 100, 1000]}]

scores = ['accuracy', 'precision_macro', 'recall_macro', 'f1_macro']

for score in scores:
    print("# Tuning hyper-parameters for %s" % score)
    clf = GridSearchCV(
        SVC(verbose = False), tuned_parameters, scoring=score
    )
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:")
    print(clf.best_params_)
    print("Grid scores on development set:")
    means = clf.cv_results_['mean_test_score']
    stds = clf.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, clf.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()

print('------------------------------------------------------------')
print('SVC output:')
clf    = SVC(C = 1000, verbose = True, kernel = "rbf", gamma=0.0001) #prints data

model  = clf.fit(X_train, y_train)
score  = clf.score(X_test, y_test)
y_pred = clf.predict(X_test)

print('\n') #needed because SVC prints output in a weird way
print('SVC Model:')
print(model)
print('Score: {}\n'.format(score))

precision = precision_score(y_test, y_pred, average='macro', zero_division=1)
recall = recall_score(y_test, y_pred, average='macro', zero_division=1)
f1 = f1_score(y_test, y_pred, average='macro', zero_division=1)

print('Precision score: {0:0.2f}'.format(precision))
print('Recall score: {0:0.2f}'.format(recall))
print('F1 score: {0:0.2f}'.format(f1))

