from output_top_tfidfs import output_top_tfidfs
from mk_doc_vectors import mk_vectors
from classification import train_svm
from numpy import save
from numpy import asarray
import json

feature_type = "words"
f =  5000 #Number of features
scores = []


print("-----------------------------------------------------------")
print("Features: ", f)
#python3 output_top_tfidfs.py [words|ngrams] [num_features_per_class].
vocab_size = output_top_tfidfs(feature_type, num_features=f)
#python3 mk_doc_vectors [words|ngrams]
num_documents, key_to_index = mk_vectors(feature_type)

# Serialize data into file:
json.dump(key_to_index, open("key_to_index.json", 'w'), indent=4)

#python3 classification.py --C=100 --kernel=linear
model, score, train1_size, train2_size = train_svm(1, "linear", 0, key_to_index, train=True)
